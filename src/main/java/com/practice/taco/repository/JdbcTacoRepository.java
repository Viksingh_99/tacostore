package com.practice.taco.repository;

import com.practice.taco.service.Taco;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public class JdbcTacoRepository implements TacoRepository{
    private JdbcTemplate jdbc;

    @Autowired
    public JdbcTacoRepository(JdbcTemplate jdbc){
        this.jdbc=jdbc;
    }

    @Override
    public Taco save(Taco taco){
        jdbc.update("");
        return new Taco();
    }
}
