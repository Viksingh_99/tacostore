package com.practice.taco.repository;

import com.practice.taco.service.Ingredient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class JdbcIngredientRepository implements IngredientsRepository {

    private JdbcTemplate jdbc;

    @Autowired
    public JdbcIngredientRepository(JdbcTemplate jdbc){
        this.jdbc = jdbc;
    }

    private Ingredient map_row_to_ingredient(ResultSet set,int rowNum) throws SQLException {
        return new Ingredient(set.getString("id"),set.getString("name"),Ingredient.Type.valueOf(set.getString("type")));
    }
    public Iterable<Ingredient> find_all(){
        return jdbc.query("select id, name, type from Ingredient",this::map_row_to_ingredient);
    }

    public Ingredient find_with_id(String id){
        return jdbc.queryForObject("select id, name, type from Ingredient where id=?",this::map_row_to_ingredient,id);
    }
    public Integer add_new_ingredient(Ingredient ingredient){
        return jdbc.update("insert into Ingredient (id, name, type) values (?, ?, ?)",ingredient.getId(), ingredient.getName(), ingredient.getType().toString());
    }
}
