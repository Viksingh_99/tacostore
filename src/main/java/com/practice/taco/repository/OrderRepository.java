package com.practice.taco.repository;

import com.practice.taco.service.Payment;

public interface OrderRepository {
    Payment save(Payment payment);
}
