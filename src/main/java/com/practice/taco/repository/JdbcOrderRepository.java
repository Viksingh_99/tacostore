package com.practice.taco.repository;

import com.practice.taco.service.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public class JdbcOrderRepository implements OrderRepository {

    private JdbcTemplate jdbc;

    @Autowired
    public JdbcOrderRepository(JdbcTemplate jdbc){
        this.jdbc=jdbc;
    }
    @Override
    public Payment save(Payment payment){
        jdbc.update("");
        return new Payment();
    }
}
