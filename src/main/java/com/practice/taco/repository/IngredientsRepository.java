package com.practice.taco.repository;

import com.practice.taco.service.Ingredient;

public interface IngredientsRepository {
    Iterable<Ingredient> find_all();
    Ingredient find_with_id(String id);
    Integer add_new_ingredient(Ingredient ingredient);
}
