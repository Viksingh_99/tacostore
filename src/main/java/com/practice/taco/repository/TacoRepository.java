package com.practice.taco.repository;

import com.practice.taco.service.Taco;

public interface TacoRepository {
    Taco save(Taco taco);
}
