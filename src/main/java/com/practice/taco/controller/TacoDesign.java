package com.practice.taco.controller;

import com.practice.taco.repository.IngredientsRepository;
import com.practice.taco.repository.JdbcIngredientRepository;
import com.practice.taco.service.Ingredient;
import com.practice.taco.service.Taco;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@Slf4j
@RequestMapping("/design")
public class TacoDesign {

    private IngredientsRepository database;

    @Autowired
    public TacoDesign(IngredientsRepository database){
        this.database = database;
    }

    @ModelAttribute
    private void addData(Model model){
        List<Ingredient> ingredient_list = new ArrayList<>();
        database.find_all().forEach(i->ingredient_list.add(i));


        for(Ingredient.Type type: Ingredient.Type.values()){
            model.addAttribute(type.toString().toLowerCase(),ingredient_list.stream().filter(p -> p.getType() == type).collect(Collectors.toList()));
        }
        return ;
    }

    @GetMapping()
    public String ingredientList(Model model) {
        List<Ingredient> ingredient_list = new ArrayList<>();
        database.find_all().forEach(i->ingredient_list.add(i));


        for(Ingredient.Type type: Ingredient.Type.values()){
            model.addAttribute(type.toString().toLowerCase(),ingredient_list.stream().filter(p -> p.getType() == type).collect(Collectors.toList()));
        }

        model.addAttribute("design", new Taco());
        return "design";
    }

    @PostMapping()
    public String ingredientsSelection(@Valid @ModelAttribute("design") Taco taco, Errors errors){
        if(errors.hasErrors()){
            return "redirect:/design";
        }
        log.info(taco.toString());
        return "redirect:/design/order";
    }

}
