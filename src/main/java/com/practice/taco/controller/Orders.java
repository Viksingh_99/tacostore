package com.practice.taco.controller;

import com.practice.taco.service.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.bind.BindResult;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping("/orders")
public class Orders {
    @GetMapping("/current")
    public String orderForm(Model model) {
        model.addAttribute("order", new Payment());
        return "orderform";
    }
    @GetMapping()
    @ResponseBody
    public String order() {
        return "Orders";
    }

    @PostMapping()
    public String processPayment(@Valid @ModelAttribute("order") Payment pay, Errors errors){

        if(errors.hasErrors()){
            return "orderform";
        }
        log.info("Payment : "+pay.toString());
        return "redirect:/";
    }
}
