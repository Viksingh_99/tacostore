package com.practice.taco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TacoStoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(TacoStoreApplication.class, args);
    }

} 
